package com.tapchishowbiz.tv.tag;

public class NewsPostTag {
	public static String TAG_ID = "id";
	public static String TAG_TITLE = "title";
	public static String TAG_DESCRIPTION = "description";
	public static String TAG_SLUG = "slug";
	public static String TAG_VIEWS = "views";
	public static String TAG_IMAGE = "image";
	public static String TAG_CREATED = "created";
}
