package com.tapchishowbiz.tv.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Scanner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class JSONfunctions {

	// constructor
	public JSONfunctions() {

	}

	public static JSONObject getJSONfromURL(String url) {
		InputStream is = null;
		String result = "";
		JSONObject jSONObject = null;
		Log.e("======> Link ", "" + url);
		// Download JSON data from URL
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
			Log.e("log_tag", "==> is " + is.toString());
		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}

		// Convert response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "UTF-8"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}

		try {

			jSONObject = new JSONObject(result);
		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
		}

		return jSONObject;
	}

	public static JSONObject readJsonDataFormUrl(String urlAddress) {
		JSONObject jsonObject = null;
		try {
			// build a URL
			urlAddress += URLEncoder.encode(urlAddress, "UTF-8");
			URL url = new URL(urlAddress);

			// read from the URL
			Scanner scan = new Scanner(url.openStream());
			String str = new String();
			while (scan.hasNext()) {
				str += scan.nextLine();
			}
			scan.close();

			str = str.toString();
			jsonObject = new JSONObject(str);
			
		} catch (UnsupportedEncodingException ex) {
			Log.e("ParseException: {}", "" + ex.getMessage());
		} catch (MalformedURLException ex) {
			Log.e("ParseException: {}", "" + ex.getMessage());
		} catch (IOException ex) {
			Log.e("ParseException: {}", "" + ex.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

}
