package com.tapchishowbiz.tv.adapter;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tapchishowbiz.tv.model.Category;
import com.tapchishowbiz.tv.service.JSONfunctions;
import com.tapchishowbiz.tv.tag.Categorytag;

import com.tapchishowbiz.tv.R;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CategoryAdapter extends BaseAdapter {

	private Context context;
	private List<Category> data;

	public CategoryAdapter(Context context, List<Category> listCategory) {
		this.context = context;
		this.data = listCategory;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Category getItem(int position) {
		if (data != null) {
			return data.get(position);
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		if (data != null) {
			return data.get(position).hashCode();
		}
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.drawer_list_item, null);
		}
		ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
		TextView txtCount = (TextView) convertView.findViewById(R.id.counter);
		Category category = data.get(position);
		if(category.getId() == "home"){
			imgIcon.setImageResource(category.getIcon());
		} else {
			imgIcon.setImageResource(R.drawable.ic_pages);
		}
		
		txtTitle.setText(category.getName());

		// Displaying count
		// Check whether it set vicible or not
		if (data.get(position).isCounterVisible()) {
			txtCount.setText(data.get(position).getCountNews());
		} else {
			txtCount.setVisibility(View.GONE);
		}

		return convertView;
	}

	public static Category convertCategoryWithJsonObject(JSONObject jsonObject)
			throws JSONException {
		String id = jsonObject.getString(Categorytag.TAG_ID);
		String name = jsonObject.getString(Categorytag.TAG_NAME);
		String slug = jsonObject.getString(Categorytag.TAG_SLUG);
		String countNews = "0";

		return new Category(id, name, slug, countNews, false);
	}
	
	public static List<Category> getCategoryByUrl(String url) {
		JSONObject jsonObject;
		List<Category> listCategory = new ArrayList<Category>();
		jsonObject = JSONfunctions.getJSONfromURL(url);
		try {
			JSONArray jsonArray;
			jsonArray = jsonObject.getJSONArray("dataservice");
			Log.e("Log_tag", ""+jsonArray.length());
			if (jsonArray != null && jsonArray.length() > 0) {
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jsonObjectCategory = jsonArray.getJSONObject(i);
					//JSONArray jsonArrayCategory = jsonObjectCategory.getJSONArray("Category");
					JSONObject ob = jsonObjectCategory.getJSONObject("Category");
					listCategory.add(CategoryAdapter.convertCategoryWithJsonObject(ob));
				}
				return listCategory;
			} else {
				return null;
			}
		} catch (Exception e) {
			Log.e("Error", e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
}
