package com.tapchishowbiz.tv.adapter;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.tapchishowbiz.tv.model.NewsPost;
import com.tapchishowbiz.tv.service.ImageLoader;
import com.tapchishowbiz.tv.service.JSONfunctions;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class NewsPostAdapter extends BaseAdapter {
	// Declare Variables
	Context context;
	LayoutInflater inflater;
	List<NewsPost> data;
	NewsPost newsPost;
	ImageLoader imageLoader;

	public NewsPostAdapter(Context context, List<NewsPost> listNewsPost) {
		this.context = context;
		data = listNewsPost;
		imageLoader = new ImageLoader(context);
	}

	@Override
	public int getCount() {
		if (data != null) {
			return data.size();
		}
		return 0;
	}

	@Override
	public NewsPost getItem(int position) {
		if (data != null) {
			return data.get(position);
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		if (data != null) {
			return data.get(position).hashCode();
		}
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		return null;
	}

	public static List<NewsPost> getNewsPostUrl( String url) {
		JSONObject jsonObject;
		List<NewsPost> listNewsPost = new ArrayList<NewsPost>();
		jsonObject = JSONfunctions.getJSONfromURL(url);
		try {
			JSONArray jsonArray;
			jsonArray = jsonObject.getJSONArray("dataNewsPost");
			if (jsonArray != null && jsonArray.length() > 0) {
				for (int i = 0; i < jsonArray.length(); i++) {
					
				}
				return listNewsPost;
			} else {
				return null;
			}
		} catch (Exception e) {
			Log.e("Error", e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
}
