package com.tapchishowbiz.tv;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class NewsPostFragment extends Fragment {
	public static String url = "";
	public NewsPostFragment() {
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.post_row_item, container,
				false);

		return rootView;
	}
	
}
