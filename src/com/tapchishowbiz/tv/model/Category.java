package com.tapchishowbiz.tv.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.tapchishowbiz.tv.tag.Categorytag;

public class Category {
	private String id;
	private String name;
	private String slug;
	private int icon;
	private String countNews;
	private boolean isCounterVisible = false;

	public Category(String name) {
		this.name = name;
	}
	public Category(String id,String name, int icon) {
		this.id = id;
		this.name = name;
		this.icon = icon;
	}
	public Category(String id, String name, String slug, String countNews,
			boolean isCounterVisible) {
		this.id = id;
		this.name = name;
		this.slug = slug;
		this.countNews = countNews;
		this.isCounterVisible = isCounterVisible;
	}

	public Category convertCategoryWithJsonObject(JSONObject jsonObject)
			throws JSONException {
		String id = jsonObject.getString(Categorytag.TAG_ID);
		String name = jsonObject.getString(Categorytag.TAG_NAME);
		String slug = jsonObject.getString(Categorytag.TAG_SLUG);
		String countNews = jsonObject.getString(Categorytag.TAG_COUNT_NEWS);

		return new Category(id, name, slug, countNews, false);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSlug() {
		return slug;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getCountNews() {
		return countNews;
	}

	public void setCountNews(String countNews) {
		this.countNews = countNews;
	}

	public boolean isCounterVisible() {
		return isCounterVisible;
	}

	public void setCounterVisible(boolean isCounterVisible) {
		this.isCounterVisible = isCounterVisible;
	}

}
