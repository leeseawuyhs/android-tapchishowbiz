package com.tapchishowbiz.tv.model;

public class NavDrawerItem {
	private int category_id;
	private int icon;
	private String title;
	private String count = "0";
	private boolean isCounterVisible;

	public NavDrawerItem() {

	}

	public NavDrawerItem(String title, int icon) {
		this.title = title;
		this.icon = icon;
	}
	public NavDrawerItem(String title, int icon, boolean isCounterVisile, String count) {
		this.title = title;
		this.icon = icon;
		this.count = count;
		this.isCounterVisible = isCounterVisile;
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public boolean getCounterVisibility() {
		return this.isCounterVisible;
	}

	public void setCounterVisibility(boolean isCounterVisible) {
		this.isCounterVisible = isCounterVisible;
	}
}
