package com.tapchishowbiz.tv.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.tapchishowbiz.tv.tag.NewsPostTag;

public class NewsPost {
	private String id;
	private String title;
	private String description;
	private String slug;
	private String image;
	private String views;
	private String created;

	public NewsPost() {
	}

	public NewsPost(String id, String title, String description, String slug,
			String image, String views, String created) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.slug = slug;
		this.image = image;
		this.views = views;
		this.created = created;
	}

	public static NewsPost convertNewsPostWithJsonObject(JSONObject jsonObject)
			throws JSONException {
		String id = jsonObject.getString(NewsPostTag.TAG_ID);
		String title = jsonObject.getString(NewsPostTag.TAG_TITLE);
		String description = jsonObject.getString(NewsPostTag.TAG_DESCRIPTION);
		String slug = jsonObject.getString(NewsPostTag.TAG_SLUG);
		String image = jsonObject.getString(NewsPostTag.TAG_IMAGE);
		String views = jsonObject.getString(NewsPostTag.TAG_VIEWS);
		String created = jsonObject.getString(NewsPostTag.TAG_CREATED);
		return new NewsPost(id, title, description, slug, image, views, created);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getViews() {
		return views;
	}

	public void setViews(String views) {
		this.views = views;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

}
